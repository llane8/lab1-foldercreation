﻿// GAME 221 Lab 01 Folder Creation   August 30, 2016
// The purpose of this script is to add a drop down menu item to the Unity top tool bar to access this script 
// Also to create multiple folders that are in a custom hierarchical structure
// And to add text documentation that explains this folder structure and how to utilize it.

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class ToolCreation : MonoBehaviour
{
    // Create a dropdown menu named Tools that will be at the top of the Unity Editor toolbar
    [MenuItem("Tool Creation/Create Folder")]

    // Always include an action after MenuItem attribute
    public static void CreateFolder()
    {
       // Materials folder
       AssetDatabase.CreateFolder("Assets","Materials");
       System.IO.File.WriteAllText(Application.dataPath+"/Materials/folderStructure.txt", "This folder is for storing Materials.");

       // Textures folder
       AssetDatabase.CreateFolder("Assets","Textures");
       System.IO.File.WriteAllText(Application.dataPath+"/Textures/folderStructure.txt", "This folder is for storing textures.");

       // Prefabs folder
       AssetDatabase.CreateFolder("Assets","Prefabs");
       System.IO.File.WriteAllText(Application.dataPath+"/Prefabs/folderStructure.txt", "This folder is for storing Prefabs.");

       // Scripts folder
       AssetDatabase.CreateFolder("Assets","Scripts");
       System.IO.File.WriteAllText(Application.dataPath+"/Scripts/folderStructure.txt", "This folder is for storing Scripts.");

       // Scenes folder
       AssetDatabase.CreateFolder("Assets","Scenes");
       System.IO.File.WriteAllText(Application.dataPath+"/Scenes/folderStructure.txt", "This folder is for storing Scenes.");

       // Animations folder
       AssetDatabase.CreateFolder("Assets","Animations");
       System.IO.File.WriteAllText(Application.dataPath+"/Animations/folderStructure.txt", "This folder is for storing raw Animations.");

       //AnimationControllers subfolder under Animations folder
       AssetDatabase.CreateFolder("Assets/Animations","AnimationControllers");
       System.IO.File.WriteAllText(Application.dataPath+"/Animations/AnimationControllers/folderStructure.txt", "This folder is for storing Animation Controllers.");

        //Refresh the Project View in the Unity environment with new changes
        AssetDatabase.Refresh();
    }
}