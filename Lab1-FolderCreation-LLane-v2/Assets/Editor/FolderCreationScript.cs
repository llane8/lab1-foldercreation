﻿// GAME 221 Lab 01 Folder Creation   August 30, 2016
// The purpose of this script is to add a drop down menu item to the Unity top tool bar to access this script 
// Also to create multiple folders that are in a custom hierarchical structure
// And to add text documentation that explains this folder structure and how to utilize it.

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class FolderCreationScript : MonoBehaviour {

    // Create a dropdown menu named Tools that will be at the top of the Unity Editor toolbar
    [MenuItem("Tools/Create Folders")]
	public static void CreateFolderStructure()
	{
	 Debug.Log("Creating your custom folder heirarchy.");

     // Dynamic Assets folder
     AssetDatabase.CreateFolder("Assets","Dynamic Assets");								
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets","Resources");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Animations");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations","Sources");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Animation Controllers");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Effects");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Models");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models","Character");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models","Environment");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Prefabs");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs","Common");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Sounds");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds","Music");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music","Common");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds","SFX");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX","Common");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources","Textures");
	 AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures","Common");
	 System.IO.File.WriteAllText(Application.dataPath+"/Dynamic Assets/Resources/folderStructure.txt", "This Dynamic Assets folder "+
                                "is for storing all resources for the Dynamic Assets (assets that are added to the game during "+
                                "run time using Resources.Load. \n\nAnimation clips are filed in the " +
                                "Animation folder. Create a new folder for each type of animation.  Animation Controllers "+
                                "from those clips are filed in the Animation Controllers folder. \n\nSpecial Effects are filed in " +
                                "the Effects folder. \n\nModels or Meshes are filed in the Models folder, in their appropriate " +
                                "subfolder, either Character or Environment. \n\nPrefabs are filed in the Prefab folder. " +
                                "Create a new subfolder in Prefabs for each scene, and file the Prefabs according to each scene they "+
                                "will be used in.  Prefabs used in multiple scenes are filed in the Common subfolder. \n\nSounds are " +
                                "filed in the Sounds folder, in their appropriate subfolder, either Music or SFX. Create new subfolders "+
                                "in Music and SFX for each scene the sounds will be used in, and file accordingly. "+
                                "If used in multiple scenes, file sounds in the appropriate Common subfolder. \n\n"+
                                "Textures are filed in the Textures folder.  Create subfolders and file according to the scenes "+
                                "in which they are used. Textures used in multiple scenes are filed in the Common subfolder.\n\n");

        // Editor folder
        //The Editor folder has already been created by hand to store this .cs file							
     System.IO.File.WriteAllText(Application.dataPath+"/Editor/folderStructure.txt", "This folder is for storing scripts that will "+
                                "be used only for the Unity Editor.");

     // Extensions folder
     AssetDatabase.CreateFolder("Assets","Extensions");							
	 System.IO.File.WriteAllText(Application.dataPath+"/Extensions/folderStructure.txt", "This folder is for storing Extensions, "+
                                "which are third party assets or asset packages. Exception is the Standard Assets folder which "+
                                "should be left as default.");

     // Gizmos folder
     AssetDatabase.CreateFolder("Assets","Gizmos");								
	 System.IO.File.WriteAllText(Application.dataPath+"/Gizmos/folderStructure.txt", "This folder is for storing Gizmo scripts.");

     // Plugins folder
     AssetDatabase.CreateFolder("Assets","Plugins");							
     System.IO.File.WriteAllText(Application.dataPath+"/Plugins/folderStructure.txt", "This folder is for storing Plugin scripts.");

     // Scripts folder
     AssetDatabase.CreateFolder("Assets","Scripts");							
	 System.IO.File.WriteAllText(Application.dataPath+"/Scripts/folderStructure.txt", "This folder is for storing Scripts. Scripts "+
                                "that share a common function are stored in the Common folder, others by level or type.");

     // Shaders folder
     AssetDatabase.CreateFolder("Assets","Shaders");							
	 System.IO.File.WriteAllText(Application.dataPath+"/Shaders/folderStructure.txt", "This folder is for storing shader scripts.");


     // Static Assets folder
     AssetDatabase.CreateFolder("Assets","Static Assets");						
	 AssetDatabase.CreateFolder("Assets/Static Assets","Animations");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Animations","Sources");
	 AssetDatabase.CreateFolder("Assets/Static Assets","Animation Controllers");
	 AssetDatabase.CreateFolder("Assets/Static Assets","Effects");
	 AssetDatabase.CreateFolder("Assets/Static Assets","Models");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Models","Character");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Models","Environment");
	 AssetDatabase.CreateFolder("Assets/Static Assets","Prefabs");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs","Common");
	 AssetDatabase.CreateFolder("Assets/Static Assets","Sounds");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Sounds","Music");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music","Common");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Sounds","SFX");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX","Common");
	 AssetDatabase.CreateFolder("Assets/Static Assets","Textures");
	 AssetDatabase.CreateFolder("Assets/Static Assets/Textures","Common");
	 System.IO.File.WriteAllText(Application.dataPath+"/Static Assets/folderStructure.txt", "This Static Assets folder " +
                                "is for storing Static Assets and all resources for the Static Assets.  \n\nAnimation clips are filed in the " +
                                "Animation folder. Create a new folder for each type of animation.  Animation Controllers " +
                                "from those clips are filed in the Animation Controllers folder. \n\nSpecial Effects are filed in " +
                                "the Effects folder. \n\nModels or Meshes are filed in the Models folder, in their appropriate " +
                                "subfolder, either Character or Environment. \n\nPrefabs are filed in the Prefab folder. " +
                                "Create a new subfolder in Prefabs for each scene, and file the Prefabs according to each scene they " +
                                "will be used in.  Prefabs used in multiple scenes are filed in the Common subfolder. \n\nSounds are " +
                                "filed in the Sounds folder, in their appropriate subfolder, either Music or SFX. Create new subfolders " +
                                "in Music and SFX for each scene the sounds will be used in, and file according to scene. " +
                                "If used in multiple scenes, file sounds in the appropriate Common subfolder. \n\n" +
                                "Textures are filed in the Textures folder.  Create subfolders and file according to the scenes " +
                                "in which they are used. Textures used in multiple scenes are filed in the Common subfolder. \n\n");

     // Testing folder
     AssetDatabase.CreateFolder("Assets","Testing");

     // Update Unity with new Project folders
     AssetDatabase.Refresh();

    }

}